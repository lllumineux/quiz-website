import os


def get_path(instance, filename):
    name, extension = os.path.splitext(filename)
    return os.path.join(instance._meta.app_label, instance._meta.model.__name__.lower(), f'file{extension}')
