def translit(text: str) -> str:
    alphabet = {
        u'а': 'a', u'б': 'b', u'в': 'v',
        u'г': 'g', u'д': 'd', u'е': 'e',
        u'ё': 'e', u'ж': 'zh', u'з': 'z',
        u'и': 'i', u'й': 'y', u'к': 'k',
        u'л': 'l', u'м': 'm', u'н': 'n',
        u'о': 'o', u'п': 'p', u'р': 'r',
        u'с': 's', u'т': 't', u'у': 'u',
        u'ф': 'f', u'х': 'h', u'ц': 'c',
        u'ч': 'ch', u'ш': 'sh', u'щ': 'sch',
        u'ь': '', u'ы': 'y', u'ъ': '',
        u'э': 'e', u'ю': 'yu', u'я': 'ya'
    }
    translated_list = []
    for source_sym in text.strip().lower():
        result_sym = alphabet.get(source_sym) or source_sym
        translated_list.append(result_sym)
    return ''.join(translated_list)
