from django.contrib import admin
from django.conf.urls.static import static
from django.urls import path

from app_account.views import AdminSignUpView
from app_main.views import quiz_view, quizzes_view, main_view, create_quiz_view
from config import settings

urlpatterns = [
    path('admin/signup/', AdminSignUpView.as_view(), name='admin_signup'),
    path('admin/', admin.site.urls),

    path('', main_view, name='main'),

    path('quizzes/create', create_quiz_view, name='create_quiz'),
    path('quizzes/', quizzes_view, name='quizzes'),
    path('quizzes/<slug:slug>/', quiz_view, name='quiz'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
