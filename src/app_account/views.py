from django.contrib.auth import login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User, Group
from django.shortcuts import redirect
from django.views.generic import CreateView


class AdminSignUpView(CreateView):
    model = User
    template_name = 'app_account/registration.html'
    form_class = UserCreationForm

    def form_valid(self, form):
        user = form.save()
        user.is_staff = True
        user.save()
        # Group "Creators" should be added to DB: all permissions for Quiz, Question, Answer, Result
        user.groups.add(Group.objects.get(name='Creators'))
        login(self.request, user)
        return redirect('admin:index')
