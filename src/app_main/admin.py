from django.contrib import admin
from django.utils.html import format_html
from django.utils.translation import gettext_lazy as _

from app_main.models import Quiz, Question, Answer, Result, Visitor, VisitorAnswer, VisitorResult
from config.settings import CORS_ALLOWED_ORIGINS


@admin.register(Quiz)
class QuizAdmin(admin.ModelAdmin):
    exclude = ('slug',)
    readonly_fields = ('author', 'link_to_quiz',)

    def link_to_quiz(self, obj):
        link = f'{CORS_ALLOWED_ORIGINS[-1]}/quizzes/{obj.slug}'
        return format_html(f'<a href="{link}" target="_blank">{link}</a>')

    link_to_quiz.short_description = _('Ссылка на квиз')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        else:
            return qs.filter(author=request.user)

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'author', None) is None:
            obj.author = request.user
        obj.save()


class AnswerInline(admin.TabularInline):
    model = Answer
    exclude = ('author',)
    extra = 0


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    exclude = ('author',)
    list_filter = ('quiz',)
    ordering = ('number',)
    inlines = (AnswerInline,)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        else:
            return qs.filter(author=request.user)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        form.base_fields['quiz'].queryset = Quiz.objects.filter(author=request.user)
        return form

    def save_formset(self, request, form, formset, change):
        if formset.model != Answer:
            return super().save_formset(request, form, formset, change)
        for form in formset.forms:
            form.instance.author = request.user
        formset.save()

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'author', None) is None:
            obj.author = request.user
        obj.save()


@admin.register(Result)
class ResultAdmin(admin.ModelAdmin):
    exclude = ('author',)
    list_filter = ('quiz',)
    filter_horizontal = ('answers',)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        else:
            return qs.filter(author=request.user)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        form.base_fields['quiz'].queryset = Quiz.objects.filter(author=request.user)
        form.base_fields['answers'].queryset = Answer.objects.filter(author=request.user)
        return form

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'author', None) is None:
            obj.author = request.user
        obj.save()


class VisitorAnswerInline(admin.TabularInline):
    model = VisitorAnswer
    extra = 0


class VisitorResultInline(admin.TabularInline):
    model = VisitorResult
    extra = 0


@admin.register(Visitor)
class VisitorAdmin(admin.ModelAdmin):
    readonly_fields = ('ip',)
    inlines = (VisitorAnswerInline, VisitorResultInline,)
