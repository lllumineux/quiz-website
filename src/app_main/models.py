from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User

from utils.helpers import translit
from utils.models import get_path


class Quiz(models.Model):
    """Квиз"""

    name = models.CharField(_('Название'), max_length=128, unique=True)
    slug = models.SlugField(_('Ключ'), unique=True)
    description = models.TextField(_('Описание'), max_length=512, blank=True, null=True)
    show = models.BooleanField(_('Показывать в общем списке'), default=False)
    author = models.ForeignKey(get_user_model(), verbose_name=_('Автор'), on_delete=models.CASCADE, editable=False)

    class Meta:
        verbose_name = _('Квиз')
        verbose_name_plural = _('Квизы')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('quiz', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        self.slug = slugify(translit(self.name))
        super().save(*args, **kwargs)


class Question(models.Model):
    """Вопрос"""

    text = models.CharField(_('Текст вопроса'), max_length=256)
    extra_text = models.TextField(_('Пояснение'), max_length=512, blank=True, null=True)
    image = models.ImageField(_('Картинка'), upload_to=get_path, blank=True, null=True)
    number = models.PositiveIntegerField(_('Номер вопроса'), default=1)
    quiz = models.ForeignKey(Quiz, verbose_name=_('Квиз'), on_delete=models.CASCADE, blank=True)
    author = models.ForeignKey(get_user_model(), verbose_name=_('Автор'), on_delete=models.CASCADE, editable=False)

    class Meta:
        verbose_name = _('Вопрос')
        verbose_name_plural = _('Вопросы')
        constraints = [models.UniqueConstraint(fields=['number', 'quiz'], name='unique_number_in_quiz')]

    def __str__(self):
        return self.text


class Answer(models.Model):
    """Ответ"""

    text = models.CharField(_('Текст ответа'), max_length=256)
    question = models.ForeignKey(Question, verbose_name=_('Вопрос'), on_delete=models.CASCADE)
    author = models.ForeignKey(get_user_model(), verbose_name=_('Автор'), on_delete=models.CASCADE, editable=False)

    class Meta:
        verbose_name = _('Ответ')
        verbose_name_plural = _('Ответы')

    def __str__(self):
        question_words = self.question.text.split()
        return f'{question_words[0]} {question_words[-1]} | {self.text}'


class Result(models.Model):
    """Результат"""

    title = models.CharField(_('Заголовок'), max_length=128)
    description = models.TextField(_('Описание'), max_length=512, blank=True, null=True)
    image = models.ImageField(_('Картинка'), upload_to=get_path, blank=True, null=True)
    quiz = models.ForeignKey(Quiz, verbose_name=_('Квиз'), on_delete=models.CASCADE)
    answers = models.ManyToManyField(Answer, verbose_name=_('Ответы приводящие к результату'), blank=True)
    author = models.ForeignKey(get_user_model(), verbose_name=_('Автор'), on_delete=models.CASCADE, editable=False)

    class Meta:
        verbose_name = _('Результат')
        verbose_name_plural = _('Результаты')

    def __str__(self):
        return self.title


class Visitor(models.Model):
    """Посетитель"""

    ip = models.CharField(_('IP'), max_length=64)
    started_quizzes = models.ManyToManyField(Quiz, verbose_name=_('Начатые квизы'), blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Посетитель')
        verbose_name_plural = _('Посетители')

    def __str__(self):
        return self.ip


class VisitorAnswer(models.Model):
    """Ответ посетителя"""

    visitor = models.ForeignKey(Visitor, verbose_name=_('Посетитель'), on_delete=models.CASCADE)
    answer = models.ForeignKey(Answer, verbose_name=_('Ответ'), on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, verbose_name=_('Квиз'), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Ответ посетителя')
        verbose_name_plural = _('Ответы посетителя')


class VisitorResult(models.Model):
    """Достигнутые результаты посетителя"""

    visitor = models.ForeignKey(Visitor, verbose_name=_('Посетитель'), on_delete=models.CASCADE)
    result = models.ForeignKey(Result, verbose_name=_('Результат'), on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, verbose_name=_('Квиз'), on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Достигнутый результат посетителя')
        verbose_name_plural = _('Достигнутые результаты посетителя')
