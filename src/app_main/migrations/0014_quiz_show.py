# Generated by Django 4.2.2 on 2023-06-16 21:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_main', '0013_alter_quiz_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='quiz',
            name='show',
            field=models.BooleanField(default=False, verbose_name='Показывать в общем списке'),
        ),
    ]
