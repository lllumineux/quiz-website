# Generated by Django 4.2.2 on 2023-06-16 06:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_main', '0005_visitor_quizzes'),
    ]

    operations = [
        migrations.AlterField(
            model_name='visitor',
            name='quizzes',
            field=models.ManyToManyField(blank=True, to='app_main.quiz', verbose_name='Начатые квизы'),
        ),
    ]
