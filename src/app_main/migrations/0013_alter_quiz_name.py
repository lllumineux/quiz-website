# Generated by Django 4.2.2 on 2023-06-16 20:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_main', '0012_rename_name_slug_quiz_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='quiz',
            name='name',
            field=models.CharField(max_length=128, unique=True, verbose_name='Название'),
        ),
    ]
