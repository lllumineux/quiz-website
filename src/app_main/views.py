from django.db.models import Count, Q, Case, When, BooleanField
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.translation import gettext_lazy as _

from app_main.forms import QuestionForm
from app_main.models import Visitor, VisitorAnswer, Question, Result, Quiz, VisitorResult
from app_main.utils import get_ip_from_request


def main_view(request, *args, **kwargs):
    """Вывод главной страницы"""

    return redirect('quizzes')


def create_quiz_view(request, *args, **kwargs):
    """Вывод страницы создания квизов"""

    return redirect('admin:index')


def quizzes_view(request, *args, **kwargs):
    """Вывод всех квизов, доступных посетителю"""

    ctx = {'quizzes': Quiz.objects.filter(show=True).order_by('id')}
    return render(request, 'app_main/quiz_list.html', context=ctx)


def quiz_view(request, *args, **kwargs):
    """Вывод текущей стадии квиза для посетителя"""

    ip = get_ip_from_request(request)
    visitor = Visitor.objects.get_or_create(ip=ip)[0]

    slug = kwargs['slug']
    quiz = get_object_or_404(Quiz, slug=slug, show=True)

    visitor_answers = VisitorAnswer.objects.filter(visitor=visitor, quiz=quiz)
    questions = Question.objects.filter(quiz=quiz)

    ctx = {}

    if request.method == 'POST':
        if 'start' in request.POST:
            visitor.started_quizzes.add(quiz)

        elif 'prev' in request.POST and visitor_answers.count() > 0:
            visitor_answers.order_by('id').last().delete()

        elif 'next' in request.POST:
            form = QuestionForm(request.POST)
            if form.is_valid():
                answer = form.cleaned_data['answer']
                VisitorAnswer.objects.create(visitor=visitor, answer_id=answer, quiz=quiz)
            else:
                ctx.update({'error': _('Ты не выбрал ответ :(')})

        elif 'restart' in request.POST:
            visitor_answers.delete()
            visitor.started_quizzes.remove(quiz)

        if 'error' not in ctx:
            return redirect('quiz', slug=slug)

    if not visitor.started_quizzes.filter(slug=slug).exists():
        ctx.update({'quiz': quiz})
        return render(request, 'app_main/start.html', context=ctx)

    if visitor_answers.count() == questions.count():
        results = (
            Result.objects
            .filter(quiz=quiz)
            .prefetch_related('answers')
            .annotate(count_answers=Count('answers', filter=Q(answers__id__in=visitor_answers.values_list('answer_id'))))
            .order_by('-count_answers')
        )
        result = results.first()
        VisitorResult.objects.get_or_create(visitor=visitor, result=result, quiz=quiz)
        # answers_stats = (
        #     visitor_answers
        #     .annotate(
        #         is_lead=Case(
        #             When(answer_id__in=result.answers.values_list('id'), then=True),
        #             default=False,
        #             output_field=BooleanField()
        #         )
        #     )
        #     .order_by('id')
        #     .values('id', 'is_lead')
        # )
        ctx.update({
            'quiz': quiz,
            'result': result,
            'results_total': results.count(),
            'results_unlocked': VisitorResult.objects.filter(visitor=visitor, quiz=quiz).count(),
            # 'answer_stats': answers_stats
        })
        return render(request, 'app_main/result.html', context=ctx)

    else:
        current_question = questions.order_by('number')[visitor_answers.count()]
        ctx.update({
            'current_question_number': visitor_answers.count() + 1,
            'total_questions_number': questions.count(),
            'question': current_question,
            'answers': current_question.answer_set.order_by('id')
        })
        return render(request, 'app_main/quiz.html', context=ctx)
