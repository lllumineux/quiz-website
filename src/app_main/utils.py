def get_ip_from_request(request):
    fwd = request.environ.get('HTTP_X_FORWARDED_FOR', None)
    if fwd is None:
        return request.environ.get('REMOTE_ADDR')
    fwd = fwd.split(',')[0]
    return fwd
