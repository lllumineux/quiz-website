from django import forms


class QuestionForm(forms.Form):
    answer = forms.CharField(max_length=256, required=True)
